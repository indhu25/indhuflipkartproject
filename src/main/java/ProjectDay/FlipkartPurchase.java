package ProjectDay;


import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class FlipkartPurchase 
{
	@Test
	public void flipkartPurchase () throws InterruptedException
	{

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		ChromeDriver driver = new ChromeDriver();

		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.get("https://www.flipkart.com/");

		Set<String> flipWindowList = driver.getWindowHandles();
		List<String> flipList = new ArrayList<>();
		flipList.addAll(flipWindowList);
		driver.switchTo().window(flipList.get(0));
		driver.findElementByXPath("//button[@class='_2AkmmA _29YdH8']").click();

		WebElement eleElectronicsLink = driver.findElementByXPath("(//span[@class='_1QZ6fC _3Lgyp8'])[1]");

		Actions gundu = new Actions(driver);
		gundu.moveToElement(eleElectronicsLink).build().perform();
		driver.findElementByXPath("(//span[@class='_1QZ6fC _3Lgyp8'])[1]").click();

		driver.findElementByLinkText("Mi").click();
		Thread.sleep(2000);

		String Mititle = driver.getTitle();
		System.out.println(Mititle);
		if(Mititle.contains("Mi Mobile Phones"))
			System.out.println("Text Matching Perfectly");
		else
			System.out.println("Title Mismatch");

		driver.findElementByXPath("(//div[@class='_1xHtJz'])[3]").click();

		Thread.sleep(5000);
		List<WebElement> ProductNameList = driver.findElementsByXPath("//div[@class='_3wU53n']");

		for(WebElement pname: ProductNameList)
		{	
			Thread.sleep(5000);
			System.out.println(pname.getText());

		}

		List<WebElement> PriceList = driver.findElementsByXPath("//div[@class='_1vC4OE _2rQ-NK']");

		for (WebElement price : PriceList) 
		{

			Thread.sleep(5000);
			System.out.println(price.getText());

		}

		driver.findElementByXPath("(//div[@class='_1vC4OE _2rQ-NK'])[1]").click();
		Thread.sleep(5000);

		Set<String> windowList2 = driver.getWindowHandles();
		List<String> SecondList = new ArrayList<>();
		SecondList.addAll(windowList2);
		driver.switchTo().window(SecondList.get(1));

		Thread.sleep(2000);

		WebElement eleFirstProduct = driver.findElementByXPath("//span[@class='_35KyD6']");
		String expectedFirstProductTitle = eleFirstProduct.getText();
		System.out.println("The Expected Title is: "+expectedFirstProductTitle);

		String FirstProductPageTitle = driver.getTitle();
		System.out.println("The Actual Title is "+FirstProductPageTitle);

		if(FirstProductPageTitle.contains(expectedFirstProductTitle))
			System.out.println("Title Matched");
		else
			System.out.println("Title Mismatch");

		WebElement eleRatingCount = driver.findElementByXPath("//span[@class='_1VpSqZ']/preceding::span[1]");
		System.out.println("The rating Count is: "+eleRatingCount.getText());

		WebElement eleReviewsCount = driver.findElementByXPath("//span[@class='_1VpSqZ']/following::span[1]");
		System.out.println("The rating Count is: "+eleReviewsCount.getText());

		driver.quit();

	}

}
